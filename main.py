import pygame

pygame.init()

width = 640
height = 480

screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("Fragmented Memories of a Lost Past")
loop = True

x = 290
y = 210
CameraX = 0
CameraY = 0
moveCameraX = 0
moveCameraY = 0

clock = pygame.time.Clock()

bg = pygame.image.load('res/bg/big_ol_bg.png')
char1 = 'res/sprites/beta.png'
beta = pygame.image.load(char1).convert_alpha()

pygame.mixer.music.load('res/music/testmoosic2.ogg')
pygame.mixer.music.play(-1)

while loop:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            loop = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                moveCameraY = -2
            if event.key == pygame.K_DOWN:
                moveCameraY = 2
            if event.key == pygame.K_LEFT:
                moveCameraX = -2
            if event.key == pygame.K_RIGHT:
                moveCameraX = 2
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_UP:
                moveCameraY = 0
            if event.key == pygame.K_DOWN:
                moveCameraY = 0
            if event.key == pygame.K_LEFT:
                moveCameraX = 0
            if event.key == pygame.K_RIGHT:
                moveCameraX = 0

    CameraX += moveCameraX
    CameraY += moveCameraY

    screen.fill((255, 255, 255))
    screen.blit(bg, (0 - CameraX, 0 - CameraY))
    screen.blit(beta, (x, y))

    pygame.display.flip()
    clock.tick(60)
